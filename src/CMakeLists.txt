add_executable(test_eeg_forward test_eeg_forward.cc)
add_executable(test_eeg_transfer test_eeg_transfer.cc)
add_executable(test_meg_transfer test_meg_transfer.cc)

set(TEST_SPHERE_FILES
  test_sphere_tet.msh
  test_sphere_tet.cond
  test_sphere_hex.dgf
  test_sphere_hex.cond
  test_sphere_electrodes.txt
  test_sphere_electrodes_transfer.txt
  test_sphere_coils.txt
  test_sphere_projections.txt
  test_sphere_dipoles.txt)

dune_symlink_to_source_files(FILES ${TEST_SPHERE_FILES})

add_system_test_per_target(TARGET test_eeg_forward
  INIFILE test_eeg_forward_cg.mini
  SCRIPT dune_outputtreecompare.py)

add_system_test_per_target(TARGET test_eeg_forward
  INIFILE test_eeg_forward_dg.mini
  SCRIPT dune_outputtreecompare.py)

add_system_test_per_target(TARGET test_eeg_transfer
  INIFILE test_eeg_transfer_cg.mini)

add_system_test_per_target(TARGET test_eeg_transfer
  INIFILE test_eeg_transfer_dg.mini)

add_system_test_per_target(TARGET test_meg_transfer
  INIFILE test_meg_transfer_cg.mini)

add_system_test_per_target(TARGET test_meg_transfer
  INIFILE test_meg_transfer_dg.mini)

if (dune-udg_FOUND)
  add_system_test_per_target(TARGET test_eeg_forward
    INIFILE test_eeg_forward_udg.mini
    SCRIPT dune_outputtreecompare.py)

  add_system_test_per_target(TARGET test_eeg_transfer
    INIFILE test_eeg_transfer_udg.mini)
endif()
